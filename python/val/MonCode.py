class Personne(object):
    def __init__(self,prenom,nom):
        self.prenom = prenom
        self.nom = nom
    def __del__(self):
        """Lorsque del est execute, il appelle __del__, puis garb. coll."""
        print "In __del__"
    def __repr__(self):
        """Pour affichage lorsque on fait nom_objet, puis return"""
        print "In __repr__"
        return "Prenom: {} et Nom: {}".format(self.prenom,self.nom)
    def __str__(self):
        """Pour affichage lorsque on fait print nom_objet, puis return"""
        print "In __str__"
        return "Pr: {} and Name: {}".format(self.prenom, self.nom)
    def __getattr__(self,nom_attribut):
        """Appelee uniquement si nom_attribut non existant"""
        #object.__getattr__(self,nom_attribut)
        print "In __getattr__"
        #raise AttributeError("Cet attribut n\'existe pas")
        # return "Attribut est: {}".format(self.nom_attribut)
    def __setattr__(self,nom_attribut,nouvelle_valeur):
        print "In __setattr__"
        object.__setattr__(self,nom_attribut,nouvelle_valeur)
    def __delattr__(self,nom_attribut):
        print "In __delattr__"
        print "{} est mort".format(self.nom_attribut)
        del self.nom_attribut
        
        
